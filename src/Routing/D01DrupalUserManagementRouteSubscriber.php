<?php

namespace Drupal\d01_drupal_user_management\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Add permissions.
 *
 * The permission "administer permissions" will give a user access
 * to add a role to a user, manage roles and change permissions.
 * These 3 things should not be managed by a single permission.
 * Drupal core is working on a solution but for now we add some extra
 * permissions of our own so we can make sure a user can add roles to
 * a newly created user without necessarily having access to the
 * permission page and the roles page.
 *
 * (https://www.drupal.org/project/drupal/issues/151311).
 */
class D01DrupalUserManagementRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Better permissions for permission management page.
    $route_paths = ['user.admin_permissions'];
    foreach ($route_paths as $key => $route_path) {
      if ($collection->get($route_path)) {
        $route = $collection->get($route_path);
        $route->setRequirement('_permission', 'administer permissions for roles');
      }
    }

    // Better permissions for roles management pages.
    $route_paths = [
      'user.admin_permissions',
      'entity.user_role.collection',
      'user.role_add',
    ];
    foreach ($route_paths as $key => $route_path) {
      if ($collection->get($route_path)) {
        $route = $collection->get($route_path);
        $route->setRequirement('_permission', 'administer roles');
      }
    }
  }

}
