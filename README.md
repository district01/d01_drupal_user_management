# d01_drupal_user_management #

The permission "administer permissions" will give a user access
to add a role to a user, manage roles and change permissions.
These 3 things should not be managed by a single permission.
Drupal core is working on a solution but for now we add some extra
permissions of our own so we can make sure a user can add roles to
a newly created user without necessarily having access to the
permission page and the roles page.

# Usage #

Configurable through the UI on the permission page.

# Release notes #

`1.0`
* Set up route subscriber to add extra permission for "administer roles"